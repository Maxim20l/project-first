const tabs = [...document.querySelectorAll('.tabs-title')];
let li = document.querySelectorAll('.tabs-content li');
let active = document.querySelector('.active');
for (let i = 0; i < tabs.length; i++) {
    if (active.dataset.name !== li[i].dataset.name) {
        li[i].style.display = 'none';
        li[i].dataset.hidden = 'true';
    }
}
tabs.forEach((element) => {
    element.addEventListener('click', (event) => {
        active.classList.remove('active');
        for (let check of tabs) {
            if (check.classList.contains('active')) {
                check.classList.remove('active');
            }
        }
        event.target.classList.add('active');
        for (let elem of li) {
            if (elem.dataset.hidden = 'true') {
                elem.style.display = 'flex';
            }
            if (event.target.dataset.name !== elem.dataset.name) {
                elem.style.display = 'none';
            }
        }
    })
})









const jobMenu = document.querySelector('.selection-of-work-categories');
const btn = document.querySelector('.btn');
const p = btn.querySelector('p');
const apear = document.querySelectorAll('.apear');
const pictures = document.getElementsByClassName('pictures');
const add = document.querySelector('.add');
let all;
jobMenu.addEventListener('click', (event) => {
    all = event.target;
    const activeItem = document.querySelector('.active-item');
    activeItem.classList.remove('active-item');
    event.target.classList.add('active-item');
    for (let elem of pictures) {
        if (elem.style.display = 'none') {
            elem.style.display = 'block'
        }
        if (event.target.dataset.offer !== elem.dataset.offer) {
            elem.style.display = 'none';
        }
        if (event.target.dataset.offers == elem.dataset.offers) {
            elem.style.display = 'block'
        }
    }
})
btn.addEventListener('click', (event) => {
    if (event.target.parentElement.classList.contains('btn')) {
        event.target.parentElement.remove()
    } else if (!event.target.parentElement.classList.contains('btn')) {
        p.parentElement.remove();
    }
    add.outerHTML = `<li class="pictures" data-offer="graphic-disign" data-offers="all"><img src="graphic design/graphic-design1.jpg" alt="umbrella">
    </li>
    <li class="pictures" data-offer="graphic-disign" data-offers="all"><img src="graphic design/graphic-design2.jpg" alt="rainbow">
    </li>
    <li class="pictures" data-offer="graphic-disign" data-offers="all"><img src="graphic design/graphic-design3.jpg" alt="cup"></li>
    <li class="pictures" data-offer="web-design" data-offers="all"><img src="web design/web-design1.jpg" alt="screen"></li>
    <li class="pictures" data-offer="web-design" data-offers="all"><img src="web design/web-design2.jpg" alt="table whith gadgets">
    </li>
    <li class="pictures" data-offer="web-design" data-offers="all"><img src="web design/web-design3.jpg" alt="laptop"></li>
    <li class="pictures" data-offer="landing-page" data-offers="all"><img src="landing page/landing-page1.jpg" alt="backyard"></li>
    <li class="pictures" data-offer="landing-page" data-offers="all"><img src="landing page/landing-page2.jpg" alt="pagewhithhands">
    </li>
    <li class="pictures" data-offer="landing-page" data-offers="all"><img src="landing page/landing-page3.jpg"
            alt="ocean whith mountain">
    </li>
    <li class="pictures" data-offer="wordpress" data-offers="all"><img src="wordpress/wordpress1.jpg" alt="media"></li>
    <li class="pictures" data-offer="wordpress" data-offers="all"><img src="wordpress/wordpress2.jpg" alt="comments"></li>
    <li class="pictures" data-offer="wordpress" data-offers="all"><img src="wordpress/wordpress3.jpg" alt="publish"></li>`;
    let active = document.querySelector('.active-item');
    let newPictures = document.getElementsByClassName('pictures');
    let jobs = document.getElementsByClassName('jobs');
    for (let i of newPictures) {
        if (i.style.display = 'none') {
            i.style.display = 'block';
        }
        for (let elem of jobs) {
            if (active.dataset.offer !== i.dataset.offer) {
                i.style.display = 'none';
            }
            if (all.dataset.offers == i.dataset.offers) {
                i.style.display = 'block'
            }
        }
    }

})
let time = new Date();
let day = time.getDate();
let month = time.getMonth();
let daySection = document.querySelectorAll('.day');
let monthSection = document.querySelectorAll('.month');
for (let elem of daySection) {
    elem.innerHTML = day;
}
let months = {
    Jan: 0,
    Feb: 1,
    Mar: 2,
    Apr: 3,
    May: 4,
    Jun: 5,
    Jul: 6,
    Aug: 7,
    Sep: 8,
    Oct: 9,
    Nov: 10,
    Dec: 11
}
let monthSelect = '';
function selectMonth() {
    for (let elem in months) {
        if (month == months[elem]) {
            monthSelect = elem;
        }

    }
}
selectMonth();
for (let elem of monthSection) {
    elem.innerHTML = monthSelect;
}
let hoverNews = document.querySelector('#grid-news')
let sectionData = document.querySelectorAll('.section-data');
let blogPost = document.querySelectorAll('.post');


let watch = [...document.querySelectorAll('.watch')];
watch.forEach((element) => {
    element.addEventListener('mouseover', () => {
        for (let div of sectionData) {
            for(let h of blogPost) {
                if (element.dataset.block == div.dataset.block && element.dataset.block == h.dataset.block) {
                    div.classList.add('hover-block');
                    h.classList.add('hover-text');
                }
            }
        }
    })
})
watch.forEach((element) => {
    element.addEventListener('mouseout', () => {
        for (let div of sectionData) {
            for(let h of blogPost) {
                if (element.dataset.block == div.dataset.block && element.dataset.block == h.dataset.block) {
                    div.classList.remove('hover-block');
                    h.classList.remove('hover-text');
                }
            }
        }
    })
})


let count = 0;
let carouselImg = document.querySelectorAll('.carousel-img');
let elemenntSlider = document.querySelectorAll('.element-slider');
let leftBtn = document.getElementById('left-button');
let rightBtn = document.getElementById('right-button');
let carousel = document.getElementById('carousel');
let newActiveClass = document.querySelector('.active-img');;
carousel.addEventListener('click', (event => {

    if (event.target.classList.contains('carousel-img')) {

        let active = document.querySelector('.active-img');
        active.classList.remove('active-img');
        newActiveClass = event.target;
        newActiveClass.classList.add('active-img');
        for (let elem of elemenntSlider) {
            if (newActiveClass.dataset.slider == elem.dataset.slider) {
                let visible = document.querySelector('.visible');
                visible.classList.remove('visible');
                visible.classList.add('unvisible');
                elem.classList.remove('unvisible');
                elem.classList.add('visible');
            }
        }
    }
}))
rightBtn.addEventListener('click', (event) => {
    for (let i = 0; i < carouselImg.length; i++) {
        if (carouselImg[i].classList.contains('active-img')) {
            count = i;
        }
    }
    newActiveClass.classList.remove('active-img');
    if (count + 1 == carouselImg.length) {
        count = 0;
    } else { count++ }
    carouselImg[3].classList.remove('active-img');
    carouselImg[count].classList.add('active-img');
    newActiveClass = document.querySelector('.active-img');
    for (let elem of elemenntSlider) {
        if (newActiveClass.dataset.slider == elem.dataset.slider) {
            let visible = document.querySelector('.visible');
            visible.classList.remove('visible');
            visible.classList.add('unvisible');
            elem.classList.remove('unvisible');
            elem.classList.add('visible');
        }
    }
    carouselImg[count - 1].classList.remove('active-img');
})
leftBtn.addEventListener('click', (event) => {
    for (let i = 0; i < carouselImg.length; i++) {
        if (carouselImg[i].classList.contains('active-img')) {
            count = i;
        }
    }
    newActiveClass.classList.remove('active-img');
    if (count - 1 == -1) {
        count = 3;
    } else { count-- }
    carouselImg[0].classList.remove('active-img');
    carouselImg[count].classList.add('active-img');
    newActiveClass = document.querySelector('.active-img');
    for (let elem of elemenntSlider) {
        if (newActiveClass.dataset.slider == elem.dataset.slider) {
            let visible = document.querySelector('.visible');
            visible.classList.remove('visible');
            visible.classList.add('unvisible');
            elem.classList.remove('unvisible');
            elem.classList.add('visible');
        }
    }
    carouselImg[count - 1].classList.remove('active-img');
})